﻿/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Core;
using OpenNos.DAL.EF;
using OpenNos.DAL.EF.Helpers;
using OpenNos.DAL.Interface;
using OpenNos.Data;
using OpenNos.Data.Enums;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenNos.DAL.DAO
{
    public class RuneEffectDAO : IRuneEffectsDAO
    {
        #region Methods

        public DeleteResult DeleteByEquipmentSerialId(Guid id)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {

                    List<RuneEffects> deleteentities = context.RuneEffects.Where(s => s.EquipmentSerialId == id).ToList();
                    if (deleteentities.Count != 0)
                    {
                        context.RuneEffects.RemoveRange(deleteentities);
                        context.SaveChanges();
                    }

                    return DeleteResult.Deleted;
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("DELETE_ERROR"), id, e.Message), e);
                return DeleteResult.Error;
            }
        }

        public RuneEffectsDTO InsertOrUpdate(RuneEffectsDTO runeEffect)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    long runeEffectId = runeEffect.RuneEffectId;
                    RuneEffects entity = context.RuneEffects.FirstOrDefault(c => c.RuneEffectId.Equals(runeEffectId));

                    if (entity == null)
                    {
                        return insert(runeEffect, context);
                    }
                    return update(entity, runeEffect, context);
                }
            }
            catch (Exception e)
            {
                Logger.Error(string.Format(Language.Instance.GetMessageFromKey("INSERT_ERROR"), runeEffect, e.Message), e);
                return runeEffect;
            }
        }

        public void InsertOrUpdateFromList(List<RuneEffectsDTO> runeEffects, Guid equipmentSerialId)
        {
            try
            {
                using (OpenNosContext context = DataAccessHelper.CreateContext())
                {
                    void insert(RuneEffectsDTO runeEffect)
                    {
                        RuneEffects _entity = new RuneEffects();
                        Mapper.Mappers.RuneEffectsMapper.ToRuneEffect(runeEffect, _entity);
                        context.RuneEffects.Add(_entity);
                        context.SaveChanges();
                        runeEffect.RuneEffectId = _entity.RuneEffectId;
                    }

                    void update(RuneEffects _entity, RuneEffectsDTO runeEffect)
                    {
                        if (_entity != null)
                        {
                            Mapper.Mappers.RuneEffectsMapper.ToRuneEffect(runeEffect, _entity);
                        }
                    }

                    foreach (RuneEffectsDTO item in runeEffects)
                    {
                        item.EquipmentSerialId = equipmentSerialId;
                        RuneEffects entity = context.RuneEffects.FirstOrDefault(c => c.RuneEffectId == item.RuneEffectId);

                        if (entity == null)
                        {
                            insert(item);
                        }
                        else
                        {
                            update(entity, item);
                        }
                    }

                    context.SaveChanges();
                }
            }
            catch (Exception e)
            {
                Logger.Error(e);
            }
        }

        public IEnumerable<RuneEffectsDTO> LoadByEquipmentSerialId(Guid id)
        {
            using (OpenNosContext context = DataAccessHelper.CreateContext())
            {
                List<RuneEffectsDTO> result = new List<RuneEffectsDTO>();
                foreach (RuneEffects entity in context.RuneEffects.Where(c => c.EquipmentSerialId == id))
                {
                    RuneEffectsDTO dto = new RuneEffectsDTO();
                    Mapper.Mappers.RuneEffectsMapper.ToRuneEffectDTO(entity, dto);
                    result.Add(dto);
                }
                return result;
            }
        }

        private static RuneEffectsDTO insert(RuneEffectsDTO runeEffect, OpenNosContext context)
        {
            RuneEffects entity = new RuneEffects();
            Mapper.Mappers.RuneEffectsMapper.ToRuneEffect(runeEffect, entity);
            context.RuneEffects.Add(entity);
            context.SaveChanges();
            if (Mapper.Mappers.RuneEffectsMapper.ToRuneEffectDTO(entity, runeEffect))
            {
                return runeEffect;
            }

            return null;
        }

        private static RuneEffectsDTO update(RuneEffects entity, RuneEffectsDTO runeeffect, OpenNosContext context)
        {
            if (entity != null)
            {
                Mapper.Mappers.RuneEffectsMapper.ToRuneEffect(runeeffect, entity);
                context.SaveChanges();
            }

            if (Mapper.Mappers.RuneEffectsMapper.ToRuneEffectDTO(entity, runeeffect))
            {
                return runeeffect;
            }

            return null;
        }

        #endregion
    }
}