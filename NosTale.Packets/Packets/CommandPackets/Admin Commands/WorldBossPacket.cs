﻿////<auto-generated <- Codemaid exclusion for now (PacketIndex Order is important for maintenance)

using OpenNos.Core;
using OpenNos.Domain;

namespace NosTale.Packets.Packets.CommandPackets
{
    [PacketHeader("$WorldBoss", PassNonParseablePacket = true, Authority = AuthorityType.GameMaster)]
    public class WorldBossPacket : PacketDefinition
    {
        #region Index

        [PacketIndex(0)]
        public short MapID { get; set; }

        [PacketIndex(1)]
        public short MapX { get; set; }

        [PacketIndex(2)]
        public short MapY { get; set; }

        [PacketIndex(3)]
        public bool IsMoving { get; set; }

        [PacketIndex(4)]
        public short MonsterID { get; set; }

        #endregion

        #region Properties

        public static string ReturnHelp() => "$WorldBoss MapID MapX MapY MonsterID";

        #endregion
    }
}