/*
 * This file is part of the OpenNos Emulator Project. See AUTHORS file for Copyright information
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 */

using OpenNos.Domain;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace OpenNos.DAL.EF
{
    public class Account
    {
        #region Instantiation

        public Account()
        {
            Character = new HashSet<Character>();
            GeneralLog = new HashSet<GeneralLog>();
            PenaltyLog = new HashSet<PenaltyLog>();
        }

        #endregion

        #region Properties

        public long AccountId { get; set; }

        public AuthorityType Authority { get; set; }

        public virtual ICollection<Character> Character { get; set; }

        [MaxLength(255)]
        public string Email { get; set; }

        public virtual ICollection<GeneralLog> GeneralLog { get; set; }

        [MaxLength(255)]
        public string Name { get; set; }

        [MaxLength(255)]
        public string Password { get; set; }

        public virtual ICollection<PenaltyLog> PenaltyLog { get; set; }

        public long ReferrerId { get; set; }

        [MaxLength(45)]
        public string RegistrationIP { get; set; }

        [MaxLength(32)]
        public string VerificationToken { get; set; }

        public long BankMoney { get; set; }

        public bool DailyRewardSent { get; set; }

        public string user_security_question_1 { get; set; }

        public string user_security_question_2 { get; set; }

        public string user_security_question_3 { get; set; }

        public string user_security_answer_1 { get; set; }

        public string user_security_answer_2 { get; set; }

        public string user_security_answer_3 { get; set; }

        public string user_country { get; set; }

        public string user_frst_name { get; set; }

        public string user_lst_name { get; set; }

        public string user_mdl_name { get; set; }

        public string user_discord { get; set; }

        public string user_registration_time { get; set; }

        public string user_registration_date { get; set; }

        public float user_balance { get; set; }

        public bool UsedStarterpack { get; set; }

        #endregion
    }
}