﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Runes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.RuneEffects",
                c => new
                    {
                        RuneEffectId = c.Long(nullable: false, identity: true),
                        EquipmentSerialId = c.Guid(nullable: false),
                        Type = c.Byte(nullable: false),
                        SubType = c.Byte(nullable: false),
                        FirstData = c.Int(nullable: false),
                        SecondData = c.Int(nullable: false),
                        ThirdDada = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.RuneEffectId);
            
            AddColumn("dbo.ItemInstance", "RuneCount", c => c.Byte(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemInstance", "RuneCount");
            DropTable("dbo.RuneEffects");
        }
    }
}
