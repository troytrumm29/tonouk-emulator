﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class starterpack : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "UsedStarterpack", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "UsedStarterpack");
        }
    }
}
