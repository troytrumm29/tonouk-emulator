﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AccountModelWeb : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Account", "user_security_question_1", c => c.String());
            AddColumn("dbo.Account", "user_security_question_2", c => c.String());
            AddColumn("dbo.Account", "user_security_question_3", c => c.String());
            AddColumn("dbo.Account", "user_security_answer_1", c => c.String());
            AddColumn("dbo.Account", "user_security_answer_2", c => c.String());
            AddColumn("dbo.Account", "user_security_answer_3", c => c.String());
            AddColumn("dbo.Account", "user_country", c => c.String());
            AddColumn("dbo.Account", "user_frst_name", c => c.String());
            AddColumn("dbo.Account", "user_lst_name", c => c.String());
            AddColumn("dbo.Account", "user_mdl_name", c => c.String());
            AddColumn("dbo.Account", "user_discord", c => c.String());
            AddColumn("dbo.Account", "user_registration_time", c => c.String());
            AddColumn("dbo.Account", "user_registration_date", c => c.String());
            AddColumn("dbo.Account", "user_balance", c => c.Single(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Account", "user_balance");
            DropColumn("dbo.Account", "user_registration_date");
            DropColumn("dbo.Account", "user_registration_time");
            DropColumn("dbo.Account", "user_discord");
            DropColumn("dbo.Account", "user_mdl_name");
            DropColumn("dbo.Account", "user_lst_name");
            DropColumn("dbo.Account", "user_frst_name");
            DropColumn("dbo.Account", "user_country");
            DropColumn("dbo.Account", "user_security_answer_3");
            DropColumn("dbo.Account", "user_security_answer_2");
            DropColumn("dbo.Account", "user_security_answer_1");
            DropColumn("dbo.Account", "user_security_question_3");
            DropColumn("dbo.Account", "user_security_question_2");
            DropColumn("dbo.Account", "user_security_question_1");
        }
    }
}
