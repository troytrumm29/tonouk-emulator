﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class nt2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "BattleTowerLastStage", c => c.Byte(nullable: false));
            DropColumn("dbo.Character", "Act4ChannelCheck");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Character", "Act4ChannelCheck", c => c.Byte(nullable: false));
            DropColumn("dbo.Character", "BattleTowerLastStage");
        }
    }
}
