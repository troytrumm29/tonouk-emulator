﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patch15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "BattleTowerExp", c => c.Int(nullable: false));
            AddColumn("dbo.Character", "BattleTowerStage", c => c.Byte(nullable: false));
            AddColumn("dbo.Item", "IsDestroyable", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Item", "IsDestroyable");
            DropColumn("dbo.Character", "BattleTowerStage");
            DropColumn("dbo.Character", "BattleTowerExp");
        }
    }
}
