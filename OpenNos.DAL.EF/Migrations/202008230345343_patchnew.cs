﻿namespace OpenNos.DAL.EF.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class patchnew : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Character", "RBBWin", c => c.Long(nullable: false));
            AddColumn("dbo.Character", "RBBLose", c => c.Long(nullable: false));
            AddColumn("dbo.ItemInstance", "FusionVnum", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ItemInstance", "FusionVnum");
            DropColumn("dbo.Character", "RBBLose");
            DropColumn("dbo.Character", "RBBWin");
        }
    }
}
