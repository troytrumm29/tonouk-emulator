﻿namespace OpenNos.Domain
{
    public enum EventButtonType : byte
    {
        EquipmentEvent,
        BettingEvent,
        SpecialistEvent,
        PerfectionEvent,
        FamilyEvent,
        SealedVesselEvent,
        ExperienceEvent,
        GoldEvent,
        ReputationEvent,
        DropEvent,
        RuneEvent,
        TattooEvent
    }
}
